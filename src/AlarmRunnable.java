
public class AlarmRunnable implements Runnable {

	private Alarm alarm;
	private String pin;
	
	
	public AlarmRunnable(Alarm alarm, String pin) {
		this.alarm = alarm;
		this.pin = pin;
	}


	@Override
	public  void run() {
		alarm.enterPin(pin);
	}

}
